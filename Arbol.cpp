#include <iostream>
#include <unistd.h>
#include <fstream>
#include "Arbol.h"

using namespace std;

/*COnstructor*/
Arbol::Arbol(){}

/*Metodos*/
/*Metodo para obtener raiz del arbol*/
Nodo* Arbol::get_Raiz(){
  return this->raiz;
}

/*Metodo para crear nodo*/
void Arbol::crearNodo(int x){
  Nodo *temp;
  /*Se crea el nodo*/
  temp = new Nodo;
  /*Se le asigna un valor al nodo*/
  temp->numero = x;
  /*Se apuntara a NULL, por defecto, a ambos lados*/
  temp->izq = NULL;
  temp->der = NULL;

  /*LLamado a metodo para insertar un NOdo*/
  raiz = insertar(raiz, temp);
}

/*Metodo para insertar un Nodo*/
Nodo* Arbol::insertar(Nodo *r, Nodo *nuevoNodo){
  /*Si el nodo r esta vacio, se inserta el nodo en esa posicion*/
  if(r == NULL){
    r = nuevoNodo;
    cout << "Nodo insertado" << endl;
  }
  else{
    /*Si el nuevo valor es menor que la raiz*/
    if(nuevoNodo->numero < r->numero){
      /*Se agrega por la izq*/
      r->izq = insertar(r->izq, nuevoNodo);
    }
    /*Sino*/
    else if (nuevoNodo->numero > r->numero){
      /*Se agrega por la der*/
      r->der = insertar(r->der, nuevoNodo);
    }
    else{
      cout << "Nodo repetido" << endl;
    }
  }
  return r;
}

/*Imprimir preorden*/
void Arbol::preorden(Nodo *p){
  if(p != NULL){
    cout << "[" << p->numero << "]" << " - ";
    preorden(p->izq);
    preorden(p->der);
  }
}

/*Imprimir inorden*/
void Arbol::inorden(Nodo *p){
  if(p != NULL){
    inorden(p->izq);
    cout << "[" << p->numero << "]" << " - ";
    inorden(p->der);
  }
}

/*Imprimir postorden*/
void Arbol::postorden(Nodo *p){
  if(p != NULL){
    postorden(p->izq);
    postorden(p->der);
    cout << "[" << p->numero << "]" << " - ";
  }
}

/*Metodo que retorna el valor del nodo mas a la
  derecha respecto del nodo a eliminar*/
int Arbol::get_NumeroDer(Nodo *q){
  /*Si el puntero a la derecha es nulo*/
  if(q->der == NULL){
    return q->numero;
  }
  else{
    /*Llamado recursivo a la funcion para buscar el valor
      a la derecha*/
      return get_NumeroDer(q->der);
  }
}

/*Eliminacion de un nodo*/
Nodo* Arbol::eliminar(Nodo *r, int numero){
  /*Nodo auxiliar*/
  Nodo *aux;

  /*Si el nodo r no es nulo*/
  if(r != NULL){
    /*Si el numero del nodo es igual al que se busca*/
    if(r->numero == numero){
      /*Si apunta a nulo por ambos lados*/
      if(r->izq == NULL && r->der == NULL){
        /*Se borra el nodo y se apunta a nulo*/
        delete r;
        r = NULL;
      }
      else{ /*Si apunta nulo por der y no por izq, se asigna
        el valor del nodo mas a la der respecto del nodo
        que se eliminara*/
        numero = get_NumeroDer(r->izq);
        r->numero = numero;
        r = r->izq;
        r->der = eliminar(r->der, numero);
      }
    }
  }
  else{ /*Si el nodo es distinto del numero buscado, se sigue
    buscando. Si es mayor al numero, se busca por la izq*/
    if(r->numero > numero){
      r->izq = eliminar(r->izq, numero);
    }
    if(r->numero < numero){ /*Se busca por la der*/
      r->der = eliminar(r->der, numero);
    }

  }
  return r;
}

/*Metodo para buscar un nodo*/
bool Arbol::busqueda(Nodo *r, int numero){
  /*Si el numero es menor, se busca por la izq*/
  if(numero < r->numero){
    /*Si el nodo por la izq apunta a nulo*/
    if(r->izq == NULL){
      cout << "No existe el numero" << endl;
      return false;
    }
    else{
      /*LLamada recursiva con el valor apuntado por izq*/
      busqueda(r->izq, numero);
    }
  }

  /*Si es mauor, se busca por der*/
  if(numero > r->numero){
    /*Si apunta a nulo por derecha*/
    if(r->der == NULL){
      cout << "No existe el numero" << endl;
      return false;
    }
    else{
      /*LLamada recursiva con el valor apuntando por der*/
      busqueda(r->der, numero);
    }
  }
  if(numero == r->numero){
    cout << "Numero encontrado" << endl;
    raiz = eliminar(raiz, numero);
    cout << "Numero eliminado" << endl;
    return true;
  }
}


/*Metodo para crear el Archvivo .txt*/
void Arbol::crearGrafo(Nodo *r){
	/*Creacion del archivo*/
	ofstream archivo("grafo.txt", ios::out);
	if (archivo.fail()){
		cout << "No se pudo abrir archivo!" << endl;
	}
	else{
		/*Escritura en el archivo .txt*/
		archivo << "digraph G {\n";
		archivo << "node [style=filled fillcolor=red] ;\n" ;
		/*LLamada al metodo para ingresar la info del arbol*/
		escribirtxt(r, archivo);
		archivo << "}";
		archivo.close();
		/*Generacion de la imagen a traves del archivo .txt*/
		system("dot -Tpng -ografo.png grafo.txt &");
		/*Apertura de la imagen*/
		system("eog grafo.png &");
		}
}

/*Metodo para escribir la info del arbol en el archivo .txt*/
void Arbol::escribirtxt(Nodo *r, ofstream &archivo){
	/*Recorrido del arbol en preorden y se agregan datos al archiv .txt*/
	if( r != NULL){
		if (r->izq != NULL) {
			archivo << r->numero << "->" << r->izq->numero << ";\n";
		}
	  else {
			archivo << '"' << r->numero << "i" << '"' << " [shape=point];\n";
			archivo << r->numero << "->" << '"' << r->numero << "i" << '"' << ";\n" ;
		}
		if (r->der != NULL) {
			archivo << r->numero << "->" << r->der->numero << ";\n";
		}
		else {
			archivo << '"' << r->numero << "d" << '"' << " [shape=point];\n";
			archivo << r->numero << "->" << '"' << r->numero << "d" << '"' <<";\n";
  	}
    escribirtxt(r->der, archivo);
		escribirtxt(r->izq, archivo);
	}
}
