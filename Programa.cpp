#include <iostream>
#include "Arbol.h"

using namespace std;

/*Menu del programa*/
int Menu(){
  string opcion;
  cout << "\n------------Menu Principal----------" << endl;
  cout << "|[1] Insertar Numero al Arbol      |" << endl;
  cout << "|[2] Eliminar Numero del Arbol     |" << endl;
  cout << "|[3] Modificiar un numero del Arbol|" << endl;
  cout << "|[4] Mostrar Contenido del Arbol   |" << endl;
  cout << "|[5] Generar Grafo                 |" << endl;
  cout << "|[6] Salir                         |" << endl;
  cout << "------------------------------------" << endl;
  cout << "\nIngrese Opcion: ";
  getline(cin, opcion);

  /*Validacion si la opcion es correcta*/
  while ((stoi(opcion) > 6) || (stoi(opcion) < 1)){
    cout << "\nOPCION NO VALIDA\nIngrese Opcion:";
    getline(cin, opcion);
  }

  return stoi(opcion);
}

/*Funcion para ingresar, elegir o eliminar numeros*/
int eleccion(){
  string line;
  cout << "Numero: ";
  getline(cin, line);

  return stoi(line);

}

int main(){

  /*Variables*/
  int opcion = 0;
  int numeroEliminado = 1;
  int numeroInsertado = 1;

  Nodo *raiz = new Nodo();
  Arbol *arbol = new Arbol();

  system("clear");

  while(opcion != 6){

    opcion = Menu();
    system("clear");

    switch(opcion){
      /*Ingresar NUmeros*/
      case 1:
        cout << "----------------------------------------------" << endl;
        while(numeroInsertado != 0){
          cout << "Ingrese 0 para dejar de agregar numeros" << endl;
          numeroInsertado = eleccion();
          /*Si el numero es diferente de 0
          se llama al metodo para insertar*/
          if(numeroInsertado != 0){
            arbol->crearNodo(numeroInsertado);
          }
        }
        cout << "----------------------------------------------" << endl;
        numeroInsertado = 0;
        break;

      case 2: /*Eliminacion de un numero*/
        raiz = arbol->get_Raiz();
        /*SI el arbol esta vacio, no se puede modificar nada*/
        cout << "----------------------------------------------" << endl;
        if(raiz != NULL){
          cout << "\nNUmeros del Arbol" << endl;
          /*Impresion arbol inorden*/
          arbol->inorden(raiz);
          cout << "\n\nIngrese el numero que desea eliminar" << endl;
          numeroEliminado = eleccion();
          /*Se envia el numero al metodo de busqueda*/
          arbol->busqueda(raiz, numeroEliminado);
          cout << "\n----------------------------------------------" << endl;
        }
        else{
          cout << "Arbol Vacio" << endl;
          cout << "----------------------------------------------" << endl;
        }
        numeroEliminado = 1;
        break;

      case 3: /*Modificiar numeros*/
        raiz = arbol->get_Raiz();
        /*SI el arbol esta vacio, no se puede modificar nada*/
        cout << "----------------------------------------------" << endl;
        if(raiz != NULL){
          cout << "\nNUmeros del Arbol" << endl;
          /*Impresion arbol inorden*/
          arbol->inorden(raiz);
          cout << "\n\nIngrese el numero que desea modificar" << endl;
          numeroEliminado = eleccion();
          cout << "\nIngrese Numero que desea ingresar" << endl;
          numeroInsertado = eleccion();

          /*Si el numero a eliminar es encontrado y es eliminado*/
          if(arbol->busqueda(raiz, numeroEliminado) == true){
            /*Se cambia por el numero nuevo*/
            arbol->crearNodo(numeroInsertado);
            cout << "\nModificacion Exitosa!" << endl;
          }
          cout << "\n----------------------------------------------" << endl;
        }
        else{
          cout << "Arbol Vacio" << endl;
          cout << "----------------------------------------------" << endl;
        }
        numeroEliminado = 1;
        numeroInsertado = 1;
        break;

      case 4: /*Mostrar el arbol;*/
        raiz = arbol->get_Raiz();
        /*SI el arbol esta vacio, no se puede modificar nada*/
        cout << "----------------------------------------------" << endl;
        if(raiz != NULL){
          cout << "\nContenido del Arbol" << endl;
          /*Impresion arbol inorden*/
          cout << "\nInorden\n" << endl;
          arbol->inorden(raiz);
          /*Impresion arbol preorden*/
          cout << "\n\nPreorden\n" << endl;
          arbol->preorden(raiz);
          /*Impresion arbol postorden*/
          cout << "\n\nPostorden\n" << endl;
          arbol->postorden(raiz);
          cout << "\n----------------------------------------------" << endl;
        }
        else{
          cout << "Arbol Vacio" << endl;
          cout << "----------------------------------------------" << endl;
        }
        break;

      case 5:  /*Crear el Grafo*/
        raiz = arbol->get_Raiz();
        if(raiz != NULL){
          arbol->crearGrafo(raiz);
        }
        else{
          cout << "Arbol vacio" << endl;
        }
        break;

      case 6: /*Salir del programa*/
        cout << "¡Fin del programa!" << endl;
        break;
    }
  }





























  return 0;
}
