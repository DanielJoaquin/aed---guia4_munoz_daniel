#include <iostream>
using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

typedef struct Nodo{
  int numero=0;
  struct Nodo *izq;
  struct Nodo *der;
} Nodo;

class Arbol{
  private:
    Nodo *raiz = NULL;
  public:
    /*Constructor*/
    Arbol();
    /*Metodos*/
    Nodo *get_Raiz();
    void crearNodo(int x);
    Nodo *insertar(Nodo *r, Nodo *nuevoNodo);
    void preorden(Nodo *p);
    void inorden(Nodo *p);
    void postorden(Nodo *p);
    int get_NumeroDer(Nodo *q);
    Nodo *eliminar(Nodo *r, int numero);
    bool busqueda(Nodo *r, int numero);
    void crearGrafo(Nodo *r);
    void escribirtxt(Nodo *r, ofstream &archivo);


};
#endif
